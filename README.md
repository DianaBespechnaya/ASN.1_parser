# ASN.1 parser C++
---
## Description
Provides you to parse data encoded with DER ASN.1. On the basis of the received information it builds a program structure in the form of a tree.

The implementation corresponds to [ITU-T Rec. X.690 (07/2002)](https://www.itu.int/ITU-T/studygroups/com17/languages/X.690-0207.pdf) and [ITU-T Rec. X.680 (07/2002)](https://www.itu.int/ITU-T/studygroups/com17/languages/X.680-0207.pdf).

## Installation
To run you should build asn1_decoder using VS-project (VS 2017 would be perfect). To run tests you also need to build ASN1_parser_test.Tests require Google test packages.

## Main classes
 - asn1_parser provides an interface of the parser. Use `void asn1_parser::read(uint8_t*, size_t*)` to decode information. Parser can decode part of the entered file, so you can put the length of the message you want to parse (the part starts from the beginning).
 - asn1_tree_analyzer provides an interface for working with the resulted ASN.1 tree. Use `void asn1_tree_analyzer::print_tree(bool = false, FILE* = nullptr)` for printing the tree into the file or console. Also this method could show unfinished components.

## Example
Files of encoded and decoded data you can find in src/input and src/output.