#include "../asn1_decoder/asn1_types.h"
#include "../asn1_decoder/asn1_exception.h"
#include "../asn1_decoder/asn1_parser.h"

#include "pch.h"

TEST(PrimitiveTypes, EOC_type) {

	asn1_parser parser;
	uint8_t test_msg[3] = { 0x00, 0x00 };

	size_t len = 2;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "End of content");
}


TEST(PrimitiveTypes, BOOLEAN_type) {

	asn1_parser parser;
	uint8_t test_msg[3] = { 0x01, 0x01, 0xFF };

	size_t len = 3;

	parser.read(test_msg, &len);	

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "BOOLEAN = 1");
}

TEST(PrimitiveTypes, INTEGER_pos_type) {

	asn1_parser parser;
	uint8_t test_msg[5] = { 0x02, 0x03, 0x7f, 0xff, 0xff };

	size_t len = 5;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "INTEGER = 8388607");
}

TEST(PrimitiveTypes, INTEGER_neg_type) {

	asn1_parser parser;
	uint8_t test_msg[5] = { 0x02, 0x03, 0x80, 0x00, 0x01 };

	size_t len = 5;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "INTEGER = -8388607");
}

TEST(PrimitiveTypes, BIT_STRING_type) {

	asn1_parser parser;
	uint8_t test_msg[4] = {0x03, 0x02, 0x04, 0x80};

	size_t len = 4;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "BIT_STRING = 1000");
}

TEST(PrimitiveTypes, OCTET_STRING_type) {

	asn1_parser parser;
	uint8_t test_msg[10] = { 0x04, 0x08, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF };

	size_t len = 10;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "OCTET_STRING = 1000000011000100101000101110011010010001110101011011001111110111");
}

TEST(PrimitiveTypes, NULL_type) {

	asn1_parser parser;
	uint8_t test_msg[2] = { 0x05, 0x00};

	size_t len = 2;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "NULL");
}

TEST(PrimitiveTypes, OBJECT_IDENTIFIER_type) {

	asn1_parser parser;
	uint8_t test_msg[6] = { 0x06, 0x04, 0x4F,  0x83, 0x80, 0x00 };

	size_t len = 6;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "OBJECT_IDENTIFIER = 1.39.49152");
}

TEST(PrimitiveTypes, OBJECT_DESCRIPTOR_type) {

	asn1_parser parser;
	uint8_t test_msg[19] = { 0x07, 0x11, 0x4F,  0x62, 0x6a, 0x65, 0x63, 0x74, 0x20, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x6F, 0x72 };

	size_t len = 19;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "OBJECT_DESCRIPTOR = Object descriptor");
}

TEST(PrimitiveTypes, REAL_special_type) {

	asn1_parser parser;
	uint8_t test_msg[3] = { 0x09, 0x01, 0x41 };

	size_t len = 3;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "REAL = -infinity");
}

TEST(PrimitiveTypes, REAL_base8_type) {

	asn1_parser parser;
	uint8_t test_msg[5] = { 0x09, 0x03, 0x90, 0xFE, 0x02 };

	size_t len = 3;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "REAL = 2.000000 * 8 ^ -2");
}

TEST(PrimitiveTypes, REAL_base10_type) {

	asn1_parser parser;
	uint8_t test_msg[13] = { 0x09, 0x0B, 0x03, 0x2D, 0x31, 0x35, 0x36, 0x32, 0x2E, 0x35, 0x45, 0x2D, 0x35 };

	size_t len = 13;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "REAL = -1562.500000 * 10 ^ -5");
}

TEST(PrimitiveTypes, REAL_base2_type) {

	asn1_parser parser;
	uint8_t test_msg[5] = { 0x09, 0x03, 0x80, 0xfb, 0x05 };

	size_t len = 5;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "REAL = 5.000000 * 2 ^ -5");
}

TEST(PrimitiveTypes, REAL_base16_type) {

	asn1_parser parser;
	uint8_t test_msg[5] = { 0x09, 0x03, 0xa0, 0xfe, 0x28 };;

	size_t len = 5;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "REAL = 40.000000 * 16 ^ -2");
}

TEST(PrimitiveTypes, ENUMERATED_type) {

	asn1_parser parser;
	uint8_t test_msg[5] = { 0x0A, 0x03, 0x7f, 0xff, 0xff };

	size_t len = 5;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "ENUMERATED = 8388607");
}

TEST(PrimitiveTypes, ROID_type) {

	asn1_parser parser;
	uint8_t test_msg[6] = { 0x0d, 0x04, 0x4F,  0x83, 0x80, 0x00 };

	size_t len = 6;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "RELATIVE_OBJECT_IDENTIFIER = 1.39.49152");
}

TEST(PrimitiveTypes, GENERALIZEDTIME_type) {

	asn1_parser parser;
	uint8_t test_msg[17] = { 0x18, 0x0f, 0x32, 0x30, 0x32, 0x37, 0x30, 0x38, 0x31, 0x30, 0x31, 0x30, 0x30, 0x30, 0x30, 0x30,0x5a };

	size_t len = 17;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "GENERALIZEDTIME = 2027-08-10 10:00:00");
}


TEST(PrimitiveTypes, NUMERIC_STRING_type) {

	asn1_parser parser;
	uint8_t test_msg[12] = { 0x12, 0x0A, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39 };

	size_t len = 12;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "NUMERIC_STRING = 0123456789");
}

TEST(PrimitiveTypes, UTCTIME_type) {

	asn1_parser parser;
	uint8_t test_msg[15] = { 0x17, 0x0d, 0x31, 0x30, 0x31, 0x31, 0x30, 0x38, 0x31, 0x38, 0x31, 0x30, 0x34, 0x37, 0x5a };

	size_t len = 15;

	parser.read(test_msg, &len);

	std::string str = parser.release_tree()->head->block_name();
	EXPECT_EQ(str, "UTCTIME = 10-11-08 18:10:47");
}

class TestSimpleString :public ::testing::Test {
protected:
	void SetUp()
	{
		parser = std::make_unique<asn1_parser>();
		test_msg[0] = 0x00;
		test_msg[1] = 0x05;
		test_msg[2] = 0x48;
		test_msg[3] = 0x65;
		test_msg[4] = 0x6c;
		test_msg[5] = 0x6c;
		test_msg[6] = 0x6f;
	
		len = 7;
	}
	void TearDown()
	{
		
	}
	std::unique_ptr<asn1_parser> parser;
	uint8_t test_msg[7];
	size_t len;
};

TEST_F(TestSimpleString, PRINTABLESTRING_type) {
	test_msg[0] = 0x13;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "PRINTABLESTRING = Hello");
}

TEST_F(TestSimpleString, UTF8STRING_type) {
	test_msg[0] = 0x0c;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "UTF8STRING = Hello");
}

TEST_F(TestSimpleString, BMPSTRING_type) {
	test_msg[0] = 0x1E;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "BMPSTRING = Hello");
}

TEST_F(TestSimpleString, CHARSTRING_type) {
	test_msg[0] = 0x1D;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "CHARACTER STRING = Hello");
}

TEST_F(TestSimpleString, GENERALSTRING_type) {
	test_msg[0] = 0x1B;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "GENERALSTRING = Hello");
}

TEST_F(TestSimpleString, GRAPHICSTRING_type) {
	test_msg[0] = 0x19;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "GRAPHICSTRING = Hello");
}

TEST_F(TestSimpleString, VISIBLESTRING_type) {
	test_msg[0] = 0x1A;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "VISIBLESTRING = Hello");
}

TEST_F(TestSimpleString, UNIVERSALSTRING_type) {
	test_msg[0] = 0x1C;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "UNIVERSALSTRING = Hello");
}

TEST_F(TestSimpleString, IA5STRING_type) {
	test_msg[0] = 0x16;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "IA5STRING = Hello");
}

TEST_F(TestSimpleString, VIDEOTEXSTRING_type) {
	test_msg[0] = 0x15;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "VIDEOTEXSTRING = Hello");
}

TEST_F(TestSimpleString, T61STRING_type) {
	test_msg[0] = 0x14;

	parser->read(test_msg, &len);
	std::string str = parser->release_tree()->head->block_name();
	EXPECT_EQ(str, "T61STRING = Hello");
}

TEST(ConstructedTypes, IN_ONE_PRIM) {

	asn1_parser parser;
	uint8_t test_msg[7] = { 0x30, 0x05, 0x02, 0x03, 0x80, 0x00, 0x01 };

	size_t len = 7;

	parser.read(test_msg, &len);

	auto input = parser.release_tree()->head;

	std::string str = input->block_name();
	EXPECT_EQ(str, "SEQUENCE :");
	EXPECT_EQ(std::static_pointer_cast<constr_type>(input)->num_of_elements(), 1);
	EXPECT_EQ(std::static_pointer_cast<constr_type>(input)->get_items()[0]->block_name(), "INTEGER = -8388607");
}

TEST(ConstructedTypes, IN_TWO_PRIM_LINE) {
	asn1_parser parser;
	uint8_t test_msg[10] = { 0x30, 0x08, 0x02, 0x03, 0x80, 0x00, 0x01, 0x01, 0x01, 0xFF };

	size_t len = 10;

	parser.read(test_msg, &len);

	auto input = parser.release_tree()->head;

	std::string str = input->block_name();
	EXPECT_EQ(str, "SEQUENCE :");
	EXPECT_EQ(std::static_pointer_cast<constr_type>(input)->num_of_elements(), 2);
	EXPECT_EQ(std::static_pointer_cast<constr_type>(input)->get_items()[0]->block_name(), "INTEGER = -8388607");
	EXPECT_EQ(std::static_pointer_cast<constr_type>(input)->get_items()[1]->block_name(), "BOOLEAN = 1");
}

TEST(ConstructedTypes, IN_ONE_CONSTR_IN_ONE_PRIM) {
	asn1_parser parser;
	uint8_t test_msg[7] = { 0x30, 0x05, 0x30, 0x03, 0x01, 0x01, 0xFF };

	size_t len = 7;

	parser.read(test_msg, &len);

	auto input = parser.release_tree()->head;

	std::string str = input->block_name();
	EXPECT_EQ(str, "SEQUENCE :");

	auto CONSTR = std::static_pointer_cast<constr_type>(input);
	EXPECT_EQ(CONSTR->num_of_elements(), 1);
	EXPECT_EQ(CONSTR->get_items()[0]->block_name(), "SEQUENCE :");

	auto IN_CONSTR = std::static_pointer_cast<constr_type>(CONSTR->get_items()[0]);
	EXPECT_EQ(IN_CONSTR->num_of_elements(), 1);
	EXPECT_EQ(IN_CONSTR->get_items()[0]->block_name(), "BOOLEAN = 1");
}

TEST(TagTests, LONG_FORM) {
	asn1_parser parser;
	uint8_t test_msg[6] = { 0xDF, 0xC0, 0x80, 0x01, 0x01, 0x01 };
	size_t len = 6;

	parser.read(test_msg, &len);
	auto number = parser.release_tree()->head->get_tag_number();

	EXPECT_EQ(number, 1048577);
}

TEST(TagTests, CONTEXT) {
	asn1_parser parser;
	uint8_t test_msg[5] = { 0xA0, 0x03, 0x02, 0x01, 0x11 };
	size_t len = 5;
	
	parser.read(test_msg, &len);
	
	auto input = parser.release_tree()->head;

	std::string str = input->block_name();	
	EXPECT_EQ(str, "[ CONTEXT-SPECIFIC 0 ] : ");

	auto CONSTR = std::static_pointer_cast<constr_type>(input);
	EXPECT_EQ(CONSTR->num_of_elements(), 1);
	EXPECT_EQ(CONSTR->get_items()[0]->block_name(), "INTEGER = 17");
}

TEST(InvalidFile, EMPTY) {
	asn1_parser parser;
	uint8_t* test_msg = nullptr;
	size_t len = 0;

	EXPECT_THROW(parser.read(test_msg, &len), asn1_exception);
}

TEST(InvalidFile, RESERVED_TAGS) {
	asn1_parser parser;
	uint8_t test_msg[3] = { 0x0e, 0x01, 0x07 };
	size_t len = 3;

	EXPECT_THROW(parser.read(test_msg, &len), asn1_exception);
}

TEST(InvalidFile, INDEFENITE_FORM_USE) {
	asn1_parser parser;
	uint8_t test_msg[5] = { 0x02, 0x80, 0x07, 0x00, 0x00 };
	size_t len = 5;

	EXPECT_THROW(parser.read(test_msg, &len), asn1_exception);
}

TEST(CompleteTests, ID) {
	asn1_parser parser;
	uint8_t test_msg[1] = { 0xDF };
	size_t len = 1;

	parser.read(test_msg, &len);

	auto input = parser.release_tree()->head;
	bool type_complete = input->is_complete();

	EXPECT_EQ(type_complete, false);

	std::string id_fail = input->complete_analysis();
	EXPECT_EQ(id_fail, " TYPE IS NOT COMPLETED: ID BLOCK IS INCOMPLETE.");
}

TEST(CompleteTests, LENGTH) {
	asn1_parser parser;
	uint8_t test_msg[2] = { 0x02, 0x83 };
	size_t len = 2;

	parser.read(test_msg, &len);

	auto input = parser.release_tree()->head;
	bool type_complete = input->is_complete();

	EXPECT_EQ(type_complete, false);

	std::string length_fail = input->complete_analysis();
	EXPECT_EQ(length_fail, " TYPE IS NOT COMPLETED: LENGTH BLOCK IS INCOMPLETE.");
}

TEST(CompleteTests, VALUE) {
	asn1_parser parser;
	uint8_t test_msg[3] = { 0x02, 0x03, 0x08 };
	size_t len = 3;

	parser.read(test_msg, &len);

	auto input = parser.release_tree()->head;
	bool type_complete = input->is_complete();

	EXPECT_EQ(type_complete, false);

	std::string value_fail = input->complete_analysis();
	EXPECT_EQ(value_fail, " TYPE IS NOT COMPLETED: VALUE BLOCK IS INCOMPLETE.");
}
















int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}