#pragma once

#ifndef UTF8STRING_TYPE_H
#define UTF8STRING_TYPE_H

class UTF8STRING_type : public prim_type {
public:
	UTF8STRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif