#pragma once

#ifndef BIT_STRING_TYPE_H
#define BIT_STRING_TYPE_H

class BIT_STRING_type : public prim_type {
public:
	BIT_STRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::vector<bool> value;
};

#endif