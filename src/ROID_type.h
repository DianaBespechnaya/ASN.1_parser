#pragma once

#ifndef ROID_TYPE_H
#define ROID_TYPE_H

class ROID_type : public OBJECT_IDENTIFIER_type {
public:
	ROID_type();
	std::string block_name();
};

#endif