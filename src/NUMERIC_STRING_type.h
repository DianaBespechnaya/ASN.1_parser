#pragma once

#ifndef NUMERIC_STRING_TYPE_H
#define NUMERIC_STRING_TYPE_H

class NUMERIC_STRING_type : public prim_type {
public:
	NUMERIC_STRING_type();
	uint8_t* decode(uint8_t*, size_t*);
	std::string block_name();
private:
	std::string value;
};

#endif