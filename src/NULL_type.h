#pragma once

#ifndef NULL_TYPE_H
#define NULL_TYPE_H

class NULL_type : public prim_type {
public:
	NULL_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
};

#endif