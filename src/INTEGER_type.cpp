#include <cmath>
#include "asn1_types.h"
#include "asn1_exception.h"
#include "INTEGER_type.h"
#include "transform_functions.h"

INTEGER_type::INTEGER_type() {

}

std::string INTEGER_type::block_name() {
	return "INTEGER = " + std::to_string(value);
}

uint8_t* INTEGER_type::decode(uint8_t* input_data, size_t* len_of_msg) {
	
	for (int i = 0; i < length->get_inform_length()-1; i++) {
		(*len_of_msg)--;
		if (!(*len_of_msg))
			return input_data;
	}

	(*len_of_msg)--;

	block_length += length->get_inform_length();

	bool sign = (*input_data) & 0x80; // true - neg, false - pos

	uint8_t* begin = input_data;
	input_data += length->get_inform_length();

	if (sign) 
		value = convert_long_neg_hex(begin, length->get_inform_length());
	else 
		value = convert_long_hex(begin, length->get_inform_length(), 8);
	
	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}