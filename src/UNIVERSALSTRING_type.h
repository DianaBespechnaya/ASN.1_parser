#pragma once

#ifndef UNIVERSALSTRING_TYPE_H
#define UNIVERSALSTRING_TYPE_H

class UNIVERSALSTRING_type : public prim_type {
public:
	UNIVERSALSTRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif