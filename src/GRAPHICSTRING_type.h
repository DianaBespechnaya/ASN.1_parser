#pragma once

#ifndef GRAPHICSTRING_TYPE_H
#define GRAPHICSTRING_TYPE_H

class GRAPHICSTRING_type : public prim_type {
public:
	GRAPHICSTRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif