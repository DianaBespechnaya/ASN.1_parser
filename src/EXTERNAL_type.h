#pragma once

#ifndef EXTERNAL_TYPE_H
#define EXTERNAL_TYPE_H

class EXTERNAL_type : public SEQUENCE_type {
public:
	EXTERNAL_type();
	std::string block_name();
};

#endif