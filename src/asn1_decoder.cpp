// asn1_decoder.cpp: определяет точку входа для консольного приложения.
//

#include <fstream>
#include <iostream>
//#include <windows.h>
#include "asn1_types.h"
#include "asn1_exception.h"
#include "asn1_parser.h"



int main()
{
	auto parser = std::make_unique<asn1_parser>();
	auto analyzer = std::make_unique<asn1_tree_analyzer>();

	std::vector<uint8_t> vec_in;
	size_t len;

	std::string input;
	std::string fileway;

	
	while (true) {
		std::cout << "Enter filename with DER code. To close write 'exit'." << std::endl;

		std::cin >> input;

		if (input == "exit")
			return 0;

		fileway = "input\\" + input;
	
		unsigned int el;
		const char* c_fileway = fileway.c_str();

		FILE * file_in = fopen(c_fileway, "r");

		if (file_in) {
			while (fscanf(file_in, "%02x", &el) == 1)
			{
				vec_in.push_back(el);
			}
			fclose(file_in);
			len = vec_in.size();
			break;
		}
		else 
			std::cout << "Filename is incorrect!" << std::endl;
		//Sleep(40);
	}

	while (true) {
		std::cout << "If you want to decode the full file enter 'y', otherwise enter the size. Size of your file is " << len << " bytes. To close enter 'exit'." << std::endl;
		std::cin >> input;

		if (input == "y")
			break;
		else if (input == "exit")
			return 0;
		else {
			if (std::stoi(input) <= len) {
				len = std::stoi(input);
				break;
			}
			else
				std::cout << "Entered size should be smaller than the full size." << std::endl;
		}
		//Sleep(40);
	}


	parser->read(&vec_in[0], &len);

	analyzer->set_tree(parser->release_tree());

	std::cout << std::endl << "The process of parsing is finished." << std::endl << std::endl;

	while (true) {
		std::cout << "To print your tree enter 0." << std::endl;
		std::cout << "To print your tree with readiness information enter 1." << std::endl;
		std::cout << "To write your tree into the file enter 2." << std::endl;
		std::cout << "To close print 'exit'." << std::endl;

		std::cin >> input;
		if (input == "exit")
			return 0;
		switch (std::stoi(input)) {
		case 0:
				analyzer->print_tree(false);
				break;
		case 1:
				analyzer->print_tree(true);
				break;
		case 2:
			while (true) {
				std::cout << "Enter the filename. To close enter 'exit'." << std::endl;
				std::cin >> input;

				if (input == "exit")
					return 0;

				fileway = "output\\" + input;
				const char* c_fileway = fileway.c_str();
				FILE * file_in = fopen(c_fileway, "w");
				
				if (file_in) {
					while (true) {
						std::cout << "To write your tree enter 0." << std::endl;
						std::cout << "To write your tree with readiness information enter 1." << std::endl;
						std::cout << "To return enter 2." << std::endl;
						std::cout << "To close print 'exit'." << std::endl;
						std::cin >> input;

						if (input == "exit")
							return 0;

						if (std::stoi(input) == 0) {
							analyzer->print_tree(false, file_in);
							std::cout << std::endl << "The process of writing is finished." << std::endl<< std::endl;
							break;
						}	
						else if (std::stoi(input) == 1) {
							analyzer->print_tree(true, file_in);
							std::cout << std::endl << "The process of writing is finished." << std::endl << std::endl;
							break;
						}	
						else if (std::stoi(input) == 2)
							break;
						else
							std::cout << "Incorrect command." << std::endl;

						//Sleep(40);
					}

					fclose(file_in);
					break;
				}
				else
					std::cout << "Incorrect filename." << std::endl;

				//Sleep(40);
			}
			break;

		default:
			std::cout << "Incorrect command." << std::endl;
		}
		//Sleep(40);
	}

    return 0;
}

