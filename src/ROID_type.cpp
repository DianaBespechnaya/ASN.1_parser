#include <string>
#include "asn1_types.h"
#include "OBJECT_IDENTIFIER_type.h"
#include "ROID_type.h"

ROID_type::ROID_type() {

}

std::string ROID_type::block_name() {
	std::string result;

	for (auto bit : OBJECT_IDENTIFIER_type::value)
		result += std::to_string(bit) + ".";

	if(result.size())
		result.pop_back();

	return "RELATIVE_OBJECT_IDENTIFIER = " + result;
}