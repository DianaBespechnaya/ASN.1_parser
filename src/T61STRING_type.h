#pragma once

#ifndef T61STRING_TYPE_H
#define T61STRING_TYPE_H

class T61STRING_type : public prim_type {
public:
	T61STRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif