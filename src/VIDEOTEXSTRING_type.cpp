#include <string>
#include "asn1_types.h"
#include "asn1_exception.h"
#include "VIDEOTEXSTRING_type.h"

VIDEOTEXSTRING_type::VIDEOTEXSTRING_type() {

}

std::string VIDEOTEXSTRING_type::block_name() {
	return "VIDEOTEXSTRING = " + value;
}

uint8_t* VIDEOTEXSTRING_type::decode(uint8_t* input_data, size_t* len_of_msg) {
	if (!(*len_of_msg))
		return input_data;

	for (uint8_t octet = 0; octet < length->get_inform_length(); octet++) {
		if (!(*len_of_msg))
			return input_data;

		value += *input_data;

		input_data++;
		block_length++;
		(*len_of_msg)--;
	}

	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}