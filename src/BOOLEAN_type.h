#pragma once

#ifndef BOOLEAN_TYPE_H
#define BOOLEAN_TYPE_H

class BOOLEAN_type : public prim_type {
public:
	BOOLEAN_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	bool value;
};

#endif