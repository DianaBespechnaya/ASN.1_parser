#pragma once

#ifndef ASN1_TYPES_H
#define ASN1_TYPES_H

#include <cstdint>
#include <vector>
#include <memory>

class ASN1_block {
public:
	ASN1_block();
	size_t get_length();
	virtual uint8_t* decode(uint8_t*, size_t*) = 0;
	bool is_complete();
	
protected:
	size_t block_length;
	bool complete;
};

class id_block : public ASN1_block {
public:
	uint8_t* decode(uint8_t*, size_t*);
	unsigned long long get_tag_number();
	char get_tag_class();
	bool get_format();
	id_block();
private:
	char tag_class;
	unsigned long long tag_number;
	bool format; // 1 - constructed, 0 - primitive 
};

class length_block : public ASN1_block {
public:
	length_block();
	uint8_t* decode(uint8_t*, size_t*);
	size_t get_inform_length();
private:
	size_t inform_length;
	bool length_form; // 1- longform, 0 - shortform
	bool indefenite_form; // 1 - yes, 0 - no
};

class asn1_parser;
class asn1_tree;
class asn1_tree_analyzer;

class type : public ASN1_block {
public:
	type();
	size_t length_completed();
	std::string complete_analysis();
	uint8_t* decode(uint8_t*, size_t*);
	unsigned long long get_tag_number();
	virtual ~type();
	virtual std::string block_name();
	friend uint8_t* make_type(uint8_t*, size_t*, std::shared_ptr<type>&);
	template <class T> 
	friend uint8_t* make_value(uint8_t*, size_t*, std::shared_ptr<type>&);

	friend asn1_parser;
	friend asn1_tree;
	friend asn1_tree_analyzer;
protected:
	
	std::unique_ptr<id_block> id;
	std::unique_ptr<length_block> length;
};

class prim_type : public type {
public:
	prim_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::vector<uint8_t> value;
};

class constr_type : public type {
public:
	constr_type();
	virtual ~constr_type();
	uint8_t* decode(uint8_t*, size_t*);
	size_t num_of_elements();
	std::vector<std::shared_ptr<type>> get_items();
	friend asn1_tree_analyzer;
	std::string block_name();
private:
	std::vector<std::shared_ptr<type>> value;
};


#endif