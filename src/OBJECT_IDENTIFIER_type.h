#pragma once


#ifndef OBJECT_IDENTIFIER_TYPE_H
#define OBJECT_IDENTIFIER_TYPE_H

class OBJECT_IDENTIFIER_type : public prim_type {
public:
	OBJECT_IDENTIFIER_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
protected:
	std::vector<uint64_t> value;
};

#endif