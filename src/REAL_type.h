#pragma once

#ifndef REAL_TYPE_H
#define REAL_TYPE_H

class REAL_type : public prim_type {
public:
	REAL_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	long double mantissa;
	uint8_t base; //2, 8, 10, 16
	int64_t exponent;
	
	uint8_t NR_form; // NR1, NR2, NR3
	std::string special_char; // +/- infinity

	bool get_special; // special_char
};

#endif