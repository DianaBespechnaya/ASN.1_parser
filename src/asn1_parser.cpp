#include <iostream>
#include <cstdint>
#include <vector>
#include <memory>


#include "asn1_types.h"
#include "asn1_exception.h"
#include "EOC_type.h"
#include "BOOLEAN_type.h"
#include "INTEGER_type.h"
#include "OBJECT_IDENTIFIER_type.h"
#include "REAL_type.h"
#include "OBJECT_DESCRIPTOR_type.h"
#include "UTF8STRING_type.h"
#include "OCTET_STRING_type.h"
#include "BIT_STRING_type.h"
#include "ENUMERATED_type.h"
#include "ROID_type.h"
#include "NUMERIC_STRING_type.h"
#include "IA5STRING_type.h"
#include "PRINTABLESTRING_type.h"
#include "VISIBLESTRING_type.h"
#include "VIDEOTEXSTRING_type.h"
#include "UNIVERSALSTRING_type.h"
#include "TELETEXSTRING_type.h"
#include "GRAPHICSTRING_type.h"
#include "GENERALSTRING_type.h"
#include "BMPSTRING_type.h"
#include "CHARSTRING_type.h"
#include "T61STRING_type.h"
#include "UTCTIME_type.h"
#include "GENERALIZEDTIME_type.h"
#include "SEQUENCE_type.h"
#include "EXTERNAL_type.h"
#include "EMBEDDEDPDV_type.h"
#include "SET_type.h"
#include "NULL_type.h"
#include "asn1_parser.h"


template <class T>
void asn1_tree::set_head(uint8_t* input_data, size_t* len_of_msg, std::shared_ptr<type>& new_head) {
	head = std::shared_ptr<type>(new T());

	head->id = std::unique_ptr<id_block>(new id_block());
	*(head->id) = *(new_head->id);
	head->block_length += head->id->get_length();

	head->length = std::unique_ptr<length_block>(new length_block());
	*(head->length) = *(new_head->length);
	head->block_length += head->length->get_length();

	if (head->id->get_tag_class() == 1) // UNIVERSAL
		if (!(head->id->get_tag_number() == 16 || head->id->get_tag_number() == 17 || head->id->get_tag_number() == 8 || head->id->get_tag_number() == 11) && head->id->get_format())
			throw asn1_exception("Only SET, SEQUENCE, EXTERANAL and EMBEDDED PDV types can be constructed.");

	if (!(*len_of_msg))
		return;

	head->decode(input_data, len_of_msg);
}

asn1_tree::asn1_tree() : max_depth(0) {
	//std::cout << "asn_tree ctor" << std::endl;
}


asn1_parser::asn1_parser() {
	//std::cout << "parser ctor" << std::endl;
}

asn1_parser::~asn1_parser() {
	
	//std::cout << "parser dctor" << std::endl;
}

void asn1_parser::read(uint8_t* input_data, size_t* len_of_msg) {
	if (!(*len_of_msg))
		throw asn1_exception("Size of DER file = 0.");

	// init tree with head
	auto head_type = std::make_shared<type>();

	tree = std::unique_ptr<asn1_tree>(new asn1_tree());

	head_type->id = std::unique_ptr<id_block>(new id_block());
	uint8_t* end_of_id = head_type->id->decode(input_data, len_of_msg);

	head_type->length = std::unique_ptr<length_block>(new length_block());
	uint8_t* end_of_len = head_type->length->decode(end_of_id, len_of_msg);

	//set the head and decode inf

	switch (head_type->id->get_tag_class()) {
	case 1: // UNIVERSAL
		switch (head_type->id->get_tag_number()) {
		case 0:
			return tree->set_head< EOC_type >(end_of_len, len_of_msg, head_type);
			break;
		case 1:
			return tree->set_head< BOOLEAN_type >(end_of_len, len_of_msg, head_type);
			break;

		case 2:
			return tree->set_head< INTEGER_type >(end_of_len, len_of_msg, head_type);
			break;

		case 3:
			return tree->set_head< BIT_STRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 4:
			return tree->set_head< OCTET_STRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 5:
			return tree->set_head< NULL_type >(end_of_len, len_of_msg, head_type);
			break;

		case 6:
			return tree->set_head< OBJECT_IDENTIFIER_type >(end_of_len, len_of_msg, head_type);
			break;

		case 7:
			return tree->set_head< OBJECT_DESCRIPTOR_type >(end_of_len, len_of_msg, head_type);
			break;

		case 8:
			return tree->set_head< EXTERNAL_type >(end_of_len, len_of_msg, head_type);
			break;

		case 9:
			return tree->set_head< REAL_type >(end_of_len, len_of_msg, head_type);
			break;

		case 10:
			return tree->set_head< ENUMERATED_type >(end_of_len, len_of_msg, head_type);
			break;

		case 11:
			return tree->set_head< EMBEDDEDPDV_type >(end_of_len, len_of_msg, head_type);
			break;

		case 12:
			return tree->set_head< UTF8STRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 13:
			return tree->set_head< ROID_type >(end_of_len, len_of_msg, head_type);
			break;

		case 14:
		case 15:
			throw asn1_exception("Tag numbers 14 and 15 are reserved.");


		case 16:
			return tree->set_head< SEQUENCE_type >(end_of_len, len_of_msg, head_type);
			break;

		case 17:
			return tree->set_head< SET_type >(end_of_len, len_of_msg, head_type);
			break;

		case 18:
			return tree->set_head< NUMERIC_STRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 19:
			return tree->set_head< PRINTABLESTRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 20:
			return tree->set_head< T61STRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 21:
			return tree->set_head< VIDEOTEXSTRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 22:
			return tree->set_head< IA5STRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 23:
			return tree->set_head< UTCTIME_type >(end_of_len, len_of_msg, head_type);
			break;

		case 24:
			return tree->set_head< GENERALIZEDTIME_type >(end_of_len, len_of_msg, head_type);
			break;

		case 25:
			return tree->set_head< GRAPHICSTRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 26:
			return tree->set_head< VISIBLESTRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 27:
			return tree->set_head< GENERALSTRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 28:
			return tree->set_head< UNIVERSALSTRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 29:
			return tree->set_head< CHARSTRING_type >(end_of_len, len_of_msg, head_type);
			break;

		case 30:
			return tree->set_head< BMPSTRING_type >(end_of_len, len_of_msg, head_type);
			break;

		}
		break;
		case 2: // APPLICATION
		case 3: // CONTEXT-SPECIFIC
		case 4: // PRIVATE
			if (head_type->id->get_format()) // if constructed
				return tree->set_head< constr_type >(end_of_len, len_of_msg, head_type);
			else
				return tree->set_head< prim_type >(end_of_len, len_of_msg, head_type);
			break;
	}
	

}


std::unique_ptr<asn1_tree>&& asn1_parser::release_tree() {
	return std::move(tree);
}

asn1_tree_analyzer::asn1_tree_analyzer() {

}

void asn1_tree_analyzer::set_tree(std::unique_ptr<asn1_tree> new_tree) {
	tree = std::unique_ptr<asn1_tree>(std::move(new_tree));
}

void asn1_tree_analyzer::print_constr_type(std::shared_ptr<constr_type> const& node, uint64_t level, bool completed, FILE* file) {
	std::string raw;

	for (int i = 0; i < level; i++)
		raw += "   ";

	raw += node->block_name();
		
	raw += " (" + std::to_string(node->num_of_elements());

	if (node->num_of_elements() > 1)
		raw +=  " elements)";
	else
		raw += " element)";

	if (completed)
		if (node->is_complete() == false)
				raw += node->complete_analysis();
			
	if (file) {
		fputs(raw.c_str(), file);
		fputs("\n", file);
	}
	else
		std::cout << raw << std::endl;

	level++;

	for (auto child : node->value) {
		print_tree(child, level, completed, file);
	}
}

void asn1_tree_analyzer::print_tree(bool completed, FILE* file) {
	if (tree == nullptr)
		throw asn1_exception("Tree is empty");
	print_tree(tree->head, 0, completed, file);
}

void asn1_tree_analyzer::print_tree(std::shared_ptr<type> const& node, uint64_t level, bool completed, FILE* file) {
	if (node->id->get_format()) {   //if constructed
		print_constr_type(std::static_pointer_cast<constr_type>(node), level, completed, file);
	}
	else {
		std::string raw;

		for (int i = 0; i < level; i++)
			raw += "   ";

		raw += node->block_name();

		if (completed)
			if (node->is_complete() == false)
					raw += node->complete_analysis();

		if (file) {
			fputs(raw.c_str(), file);
			fputs("\n", file);
		}
		else
			std::cout << raw << std::endl;
	}
}