#pragma once

#ifndef GENERALIZEDTIME_TYPE_H
#define GENERALIZEDTIME_TYPE_H

class GENERALIZEDTIME_type : public prim_type {
public:
	GENERALIZEDTIME_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif