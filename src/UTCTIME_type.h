#pragma once

#ifndef UTCTIME_TYPE_H
#define UTCTIME_TYPE_H

class UTCTIME_type : public prim_type {
public:
	UTCTIME_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif