#pragma once

#ifndef EOC_TYPE_H
#define EOC_TYPE_H

class EOC_type : public prim_type {
public:
	EOC_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
};

#endif
