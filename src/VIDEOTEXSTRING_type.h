#pragma once

#ifndef VIDEOTEXSTRING_TYPE_H
#define VIDEOTEXSTRING_TYPE_H

class VIDEOTEXSTRING_type : public prim_type {
public:
	VIDEOTEXSTRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif