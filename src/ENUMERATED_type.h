#pragma once

#ifndef ENUMERATED_TYPE_H
#define ENUMERATED_TYPE_H

class ENUMERATED_type : public INTEGER_type {
public:
	ENUMERATED_type();
	std::string block_name();
};

#endif
