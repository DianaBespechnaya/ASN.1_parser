#include <string>
#include "asn1_types.h"
#include "INTEGER_type.h"
#include "ENUMERATED_type.h"

ENUMERATED_type::ENUMERATED_type() {

}

std::string ENUMERATED_type::block_name() {
	return "ENUMERATED = " + std::to_string(INTEGER_type::value);
}
