#include <string>
#include "asn1_types.h"
#include "asn1_exception.h"
#include "IA5STRING_type.h"

IA5STRING_type::IA5STRING_type() {

}

std::string IA5STRING_type::block_name() {
	return "IA5STRING = " + value;
}

uint8_t* IA5STRING_type::decode(uint8_t* input_data, size_t* len_of_msg) {
	if (!(*len_of_msg))
		return input_data;

	for (uint8_t octet = 0; octet < length->get_inform_length(); octet++) {
		if (!(*len_of_msg))
			return input_data;

		if (*input_data > 128)
			throw asn1_exception("IA5string encoded only first 128 ASCII characters.");

		value += *input_data;

		input_data++;
		block_length++;
		(*len_of_msg)--;
	}

	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}