#include <string>
#include "asn1_types.h"
#include "asn1_exception.h"
#include "PRINTABLESTRING_type.h"

PRINTABLESTRING_type::PRINTABLESTRING_type() {

}

std::string PRINTABLESTRING_type::block_name() {
	return "PRINTABLESTRING = " + value;
}

uint8_t* PRINTABLESTRING_type::decode(uint8_t* input_data, size_t* len_of_msg) {
	if (!(*len_of_msg))
		return input_data;

	for (uint8_t octet = 0; octet < length->get_inform_length(); octet++) {
		if (!(*len_of_msg))
			return input_data;

		if (!(*input_data == 32 || 39 <= *input_data <= 41 || 43 <= *input_data <= 47 || *input_data == 58 || 65 <= *input_data <= 90 || 97 <= *input_data <= 122))
			throw asn1_exception("PRINTABLESTRING should contain only a-z, A-Z, ' () +,-.?:/= and SPACE.");
		value += *input_data;

		input_data++;
		block_length++;
		(*len_of_msg)--;
	}

	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}