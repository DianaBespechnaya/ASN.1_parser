#pragma once

#ifndef SET_TYPE_H
#define SET_TYPE_H

class SET_type : public constr_type {
public:
	SET_type();
	std::string block_name();
};

#endif
