#pragma once

#ifndef BMPSTRING_TYPE_H
#define BMPSTRING_TYPE_H

class BMPSTRING_type : public prim_type {
public:
	BMPSTRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif