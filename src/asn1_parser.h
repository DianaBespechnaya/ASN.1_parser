#pragma once

#ifndef ASN1_PARSER_H
#define ASN1_PARSER_H

class asn1_tree_analyzer;

class asn1_tree {
public:
	asn1_tree();
	template <class T>
	void set_head(uint8_t*, size_t*, std::shared_ptr<type>&);
	friend class asn1_tree_analyzer;
	
	std::shared_ptr<type> head;
	
private:
	uint64_t max_depth;
	
};


class asn1_parser {
public:
	asn1_parser();
	void read(uint8_t*, size_t*);
	std::unique_ptr<asn1_tree>&& release_tree();
	~asn1_parser();

private:
	std::unique_ptr<asn1_tree> tree;
};

class asn1_tree_analyzer {
public:
	asn1_tree_analyzer();
	void print_tree(bool = false, FILE* = nullptr);
	void print_tree(std::shared_ptr<type>const&, uint64_t = 0, bool = false, FILE* = nullptr);
	void set_tree(std::unique_ptr<asn1_tree> new_tree);

private:
	void print_constr_type(std::shared_ptr<constr_type> const&, uint64_t = 0, bool = false, FILE* = nullptr);
	std::unique_ptr<asn1_tree> tree;
};
#endif