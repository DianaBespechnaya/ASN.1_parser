#include <memory>
#include "transform_functions.h"

unsigned long long convert_long_hex(uint8_t* hex, unsigned long long count, int shift) {
	unsigned long long result = 0;
	uint8_t octet;
	uint8_t mask = 0;
	uint8_t position = 1;

	for (int i = 0; i < shift; i++) {
		mask += position;
		position *= 2;
	}

	for (int i = 0; i < count; i++) {
		octet = (*hex) & mask;
		result = result << shift;
		result += octet;
		hex++;
	}

	return result;
}

long long convert_long_neg_hex(uint8_t* hex, unsigned long long count) {
	int64_t input_int = 0;

	for (uint8_t octet = 0; octet < count; octet++) {
		for (uint8_t bit = 0; bit < 8; bit++) {
			uint8_t position = pow(2, bit);
			bool current_bit = (*hex) & position;
			int shift = count - 1 - octet;
			input_int += current_bit * pow(2, bit + 8 * shift); // decoding from the left side
		}
		hex++;
	}

	int64_t int_to_xor = pow(2, 8 * (count)) - 1;
	long long value = (-1) * ((input_int - 1) ^ int_to_xor);

	return value;
}

