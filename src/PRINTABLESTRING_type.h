#pragma once

#ifndef PRINTABLESTRING_TYPE_H
#define PRINTABLESTRING_TYPE_H

class PRINTABLESTRING_type : public prim_type {
public:
	PRINTABLESTRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif