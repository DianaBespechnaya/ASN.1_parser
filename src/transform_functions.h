#pragma once

#ifndef TRANSFORM_FUNCTIONS_H
#define TRANSFORM_FUNCTIONS_H

unsigned long long convert_long_hex(uint8_t* hex, unsigned long long count, int shift);


long long convert_long_neg_hex(uint8_t* hex, unsigned long long count);
#endif