#pragma once

#ifndef OBJECT_DESCRIPTOR_TYPE_H
#define OBJECT_DESCRIPTORG_TYPE_H

class OBJECT_DESCRIPTOR_type : public prim_type {
public:
	OBJECT_DESCRIPTOR_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif