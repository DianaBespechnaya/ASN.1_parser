#pragma once

#ifndef TELETEXSTRING_TYPE_H
#define TELETEXSTRING_TYPE_H

class TELETEXSTRING_type : public prim_type {
public:
	TELETEXSTRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif