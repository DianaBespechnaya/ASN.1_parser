#include <string>
#include "asn1_types.h"
#include "asn1_exception.h"
#include "UTCTIME_type.h"

UTCTIME_type::UTCTIME_type() {

}

std::string UTCTIME_type::block_name() {
	if (value.length() != 13 || value[value.length() - 1] != 'Z')
		throw asn1_exception("DER encoding of UTCTIME should be YYMMDDHHMMSSZ");

	std::string year = value.substr(0, 2);
	std::string month = value.substr(2, 2);
	std::string day = value.substr(4, 2);
	std::string hour = value.substr(6, 2);
	std::string minute = value.substr(8, 2);
	std::string seconds = value.substr(10, 2);
	return "UTCTIME = " + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + seconds;
}

uint8_t* UTCTIME_type::decode(uint8_t* input_data, size_t* len_of_msg) {
	if (!(*len_of_msg))
		return input_data;

	for (uint8_t octet = 0; octet < length->get_inform_length(); octet++) {
		if (!(*len_of_msg))
			return input_data;

		value += *input_data;

		input_data++;
		block_length++;
		(*len_of_msg)--;
	}

	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}