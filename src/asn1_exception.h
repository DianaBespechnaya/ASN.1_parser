#pragma once

#ifndef ASN1_EXCEPTION_H
#define ASN1_EXCEPTION_H

#include <exception>
#include <string>


class asn1_exception : public std::exception
{
public:
	explicit asn1_exception(const char* msg) throw() : message(msg) {}
	explicit asn1_exception(const std::string& msg) throw() : message(msg) {}
	~asn1_exception() throw() {}

	const char* what() const throw() { return message.c_str(); }

private:
	std::string message;
};


#endif