#pragma once

#ifndef GENERALSTRING_TYPE_H
#define GENERALSTRING_TYPE_H

class GENERALSTRING_type : public prim_type {
public:
	GENERALSTRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif