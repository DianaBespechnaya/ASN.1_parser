#pragma once

#ifndef INTEGER_TYPE_H
#define INTEGER_TYPE_H

class INTEGER_type : public prim_type {
public:
	INTEGER_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
protected:
	int64_t value;
};

#endif