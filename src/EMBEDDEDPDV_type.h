#pragma once

#ifndef EMBEDDEDPDV_TYPE_H
#define EMBEDDEDPDV_TYPE_H

class EMBEDDEDPDV_type : public SEQUENCE_type {
public:
	EMBEDDEDPDV_type();
	std::string block_name();
};

#endif