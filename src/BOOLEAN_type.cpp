#include "asn1_types.h"
#include "asn1_exception.h"
#include "BOOLEAN_type.h"

BOOLEAN_type::BOOLEAN_type() : value(false) {

}

std::string BOOLEAN_type::block_name() {
	return "BOOLEAN = " + std::to_string(value);
}

uint8_t* BOOLEAN_type::decode(uint8_t* input_data, size_t* len_of_msg) {

	if (!(*len_of_msg))
		return input_data;

	uint8_t* end_of_data = input_data;

	if ((*input_data) == 0x00) {
		value = false;
	}
	else if ((*input_data) == 0xFF) {
		value = true;
	}
	else
		throw asn1_exception("BOOLEAN type was uncorrectly encoded.");

	block_length++;
	end_of_data++;
	(*len_of_msg)--;
	
	complete = true;

	return end_of_data;
}
