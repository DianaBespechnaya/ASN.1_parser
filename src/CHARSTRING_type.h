#pragma once

#ifndef CHARSTRING_TYPE_H
#define CHARSTRING_TYPE_H

class CHARSTRING_type : public prim_type {
public:
	CHARSTRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif