#include <iostream>
#include <cstdint>
#include <vector>
#include <memory>
#include <sstream>

#include "transform_functions.h"
#include "asn1_types.h"
#include "asn1_exception.h"
#include "EOC_type.h"
#include "BOOLEAN_type.h"
#include "INTEGER_type.h"
#include "OBJECT_IDENTIFIER_type.h"
#include "REAL_type.h"
#include "OBJECT_DESCRIPTOR_type.h"
#include "UTF8STRING_type.h"
#include "UTCTIME_type.h"
#include "GENERALIZEDTIME_type.h"
#include "OCTET_STRING_type.h"
#include "BIT_STRING_type.h"
#include "ENUMERATED_type.h"
#include "ROID_type.h"
#include "NUMERIC_STRING_type.h"
#include "IA5STRING_type.h"
#include "PRINTABLESTRING_type.h"
#include "VISIBLESTRING_type.h"
#include "VIDEOTEXSTRING_type.h"
#include "UNIVERSALSTRING_type.h"
#include "TELETEXSTRING_type.h"
#include "GRAPHICSTRING_type.h"
#include "GENERALSTRING_type.h"
#include "BMPSTRING_type.h"
#include "CHARSTRING_type.h"
#include "T61STRING_type.h"
#include "SEQUENCE_type.h"
#include "EXTERNAL_type.h"
#include "EMBEDDEDPDV_type.h"
#include "SET_type.h"
#include "NULL_type.h"




ASN1_block::ASN1_block() : block_length(0), complete(false) {

}

size_t ASN1_block::get_length() {
	return block_length;
}

bool ASN1_block::is_complete() {
	return complete;
}

/////////////////////////////////////////////

id_block::id_block() : tag_class(0), tag_number(0), format(true) {

}

unsigned long long id_block::get_tag_number() {
	return tag_number;
}

uint8_t* id_block::decode(uint8_t *input_data, size_t *len_of_msg) {

	if (!(*len_of_msg)) // empty object
		return input_data;

	// find tag class
	uint8_t input_tag_class = (*input_data) & 0xC0;

	switch(input_tag_class){
	case 0x00:
		tag_class = 1; // UNIVERSAL
		break;
	case 0x40:
		tag_class = 2; // APPLICATION
		break;
	case 0x80:
		tag_class = 3; // CONTEXT-SPECIFIC
		break;
	case 0xC0:
		tag_class = 4; // PRIVATE
		break;
	}

	// find format
	uint8_t input_format = (*input_data) & 0x20;

	if (input_format)
		format = 1; // constructed
	else
		format = 0; // primitive

	// find tag number
	uint8_t input_tag_number = (*input_data) & 0x1F;

	block_length = 0; // number of blocks to decode tag number

	if (input_tag_number < 31)
		tag_number = input_tag_number;
	else
	{
		input_data++;
		(*len_of_msg)--;
		block_length++;

		if (!(*len_of_msg)) {
			return input_data;
		}

		uint8_t* begin = input_data; // first octet of the encoded tag number

		while ((*input_data) & 0x80) {
			block_length++;
			(*len_of_msg)--;
			input_data++;

			if (!(*len_of_msg)) {
				return input_data;
			}
		}

		tag_number = convert_long_hex(begin, block_length, 7); // 7 - shift, significant bits
		
	}

	uint8_t* end_of_data = ++input_data;
	(*len_of_msg)--;
	block_length++;

	complete = true;

	return end_of_data;
}

bool id_block::get_format() {
	return format;
}

char id_block::get_tag_class() {
	return tag_class;
}


/////////////////////////////////////////////////////

length_block::length_block() : inform_length(0), length_form(false), indefenite_form(false) {

}

uint8_t* length_block::decode(uint8_t* input_data, size_t* len_of_msg) {

	if ((*len_of_msg) <= 0)
		return input_data;

	uint8_t* end_of_data;

	if (*input_data == 0x80) {
		indefenite_form = true;
		throw asn1_exception("Indefenite form is used in DER");
		
		block_length = 1;

		end_of_data = ++input_data;
		(*len_of_msg)--;
	}
	else {
		length_form = (*input_data) & 0x80;

		if (length_form) {
			
			block_length = (*input_data) & 0x7F;
			for (int i = 0; i < block_length; i++) {
				(*len_of_msg)--;
				if (!(*len_of_msg))
					return input_data;
			}

			block_length++;
			end_of_data = input_data + block_length;
			input_data++;

			inform_length = convert_long_hex(input_data, block_length-1, 8); // 8 - shift, significant bits
		}
		else {
			inform_length = (*input_data) & 0x7F;
			block_length = 1;

			end_of_data = ++input_data;
			(*len_of_msg)--;
		}
	}
	complete = true;

	return end_of_data;
}

size_t length_block::get_inform_length() {
	return inform_length;
}


/////////////////////////////////////////////////////

type::type() {
	//std::cout << "type ctor" << std::endl;
}

type::~type() {
	
	//std::cout << "type dctor" << std::endl;
}

std::string type::block_name() {
	return "type";
}


uint8_t* type::decode(uint8_t* input_data, size_t* len_of_msg) {
	block_length = 0;
	//size_t __len_to_compare = *len_of_msg; //

	id = std::unique_ptr<id_block> (new id_block());

	uint8_t* end_of_id = id->decode(input_data, len_of_msg);
	block_length += id->get_length();

	length = std::unique_ptr<length_block> (new length_block());

	uint8_t* end_of_length = length->decode(end_of_id, len_of_msg);
	block_length += length->get_length();

	auto input_type = std::make_shared<type>();

	make_type(input_data, len_of_msg, input_type);

	//if ((__len_to_compare - (*len_of_msg)) == block_length) //
	//	std::cout << "COMPARE COMLETED: LENGTH IS OK" << std::endl; //

	complete = true;
	
	return end_of_length;
}

size_t type::length_completed() {
	return block_length - (id->get_length()) - (length->get_length());
}

unsigned long long type::get_tag_number() {
	return id->get_tag_number();
}

std::string type::complete_analysis() {
	if (!(id->is_complete()))
		return " TYPE IS NOT COMPLETED: ID BLOCK IS INCOMPLETE.";
	if (!(length->is_complete()))
		return " TYPE IS NOT COMPLETED: LENGTH BLOCK IS INCOMPLETE.";
	else
		return " TYPE IS NOT COMPLETED: VALUE BLOCK IS INCOMPLETE.";
}


///////////////////////////////////////////////////////////////

prim_type::prim_type() {

}

uint8_t* prim_type::decode(uint8_t* input_data, size_t* len_of_msg) {

	if (!(*len_of_msg))
		return input_data;

	for (uint8_t octet = 0; octet < length->get_inform_length(); octet++) {
		if (!(*len_of_msg))
			return input_data;

		value.push_back(*input_data);

		input_data++;
		block_length++;
		(*len_of_msg)--;
	}

	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}

std::string prim_type::block_name() {
	std::string str_tag_class;
	switch (id->get_tag_class()) {
	case 1:
		str_tag_class = "UNIVERSAL";
		break;
	case 2:
		str_tag_class = "APPLICATION";
		break;
	case 3:
		str_tag_class = "CONTEXT-SPECIFIC";
		break;
	case 4:
		str_tag_class = "PRIVATE";
		break;
	}

	std::string str_value;

	if (value.size()) {
		std::stringstream ss;
		for (auto el : value)
			ss << el << std::hex << " ";
		str_value = ss.str();
	}
	return "[ " + str_tag_class + " " + std::to_string(id->get_tag_number()) + " ] = " + str_value;
}

/////////////////////////////////////////////////////////////

constr_type::constr_type() {

}

constr_type::~constr_type() {

	//std::cout << "Constr type deleted" << std::endl;
}

size_t constr_type::num_of_elements() {
	return value.size();
}

template< class T >
uint8_t* make_value(uint8_t* input_data, size_t* len_of_msg, std::shared_ptr<type>& input_type) {
	auto new_type = std::shared_ptr<type>(new T());

	new_type->id = std::unique_ptr<id_block>(new id_block());
	*(new_type->id) = *(input_type->id);
	new_type->block_length += new_type->id->get_length();

	new_type->length = std::unique_ptr<length_block>(new length_block());
	*(new_type->length) = *(input_type->length);
	new_type->block_length += new_type->length->get_length();

	input_type = std::move(new_type);

	if ((*len_of_msg) <= 0)
		return input_data;

	uint8_t* end_of_data = input_type->decode(input_data,len_of_msg);

	return end_of_data;
}

uint8_t* make_type(uint8_t* input_data, size_t* len_of_msg, std::shared_ptr<type>& input_type) {

	input_type->id = std::unique_ptr<id_block>(new id_block());
	uint8_t* end_of_id = input_type->id->decode(input_data, len_of_msg);

	input_type->length = std::unique_ptr<length_block>(new length_block());
	uint8_t* end_of_len = input_type->length->decode(end_of_id, len_of_msg);

	switch (input_type->id->get_tag_class()) {
	case 1: // UNIVERSAL
		switch (input_type->id->get_tag_number()) {
		case 0:
			return make_value< EOC_type >(end_of_len, len_of_msg, input_type);
			break;
		case 1:
			return make_value< BOOLEAN_type >(end_of_len, len_of_msg, input_type);
			break;

		case 2:
			return make_value< INTEGER_type >(end_of_len, len_of_msg, input_type);
			break;

		case 3:
			return make_value< BIT_STRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 4:
			return make_value< OCTET_STRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 5:
			return make_value< NULL_type >(end_of_len, len_of_msg, input_type);
			break;

		case 6:
			return make_value< OBJECT_IDENTIFIER_type >(end_of_len, len_of_msg, input_type);
			break;

		case 7:
			return make_value< OBJECT_DESCRIPTOR_type >(end_of_len, len_of_msg, input_type);
			break;

		case 8:
			return make_value< EXTERNAL_type >(end_of_len, len_of_msg, input_type);
			break;

		case 9:
			return make_value< REAL_type >(end_of_len, len_of_msg, input_type);
			break;

		case 10:
			return make_value< ENUMERATED_type >(end_of_len, len_of_msg, input_type);
			break;

		case 11:
			return make_value< EMBEDDEDPDV_type >(end_of_len, len_of_msg, input_type);
			break;

		case 12:
			return make_value< UTF8STRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 13:
			return make_value< ROID_type >(end_of_len, len_of_msg, input_type);
			break;

		case 14:
		case 15:
			throw asn1_exception("Tag numbers 14 and 15 are reserved.");

		case 16:
			return make_value< SEQUENCE_type >(end_of_len, len_of_msg, input_type);
			break;

		case 17:
			return make_value< SET_type >(end_of_len, len_of_msg, input_type);
			break;

		case 18:
			return make_value< NUMERIC_STRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 19:
			return make_value< PRINTABLESTRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 20:
			return make_value< T61STRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 21:
			return make_value< VIDEOTEXSTRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 22:
			return make_value< IA5STRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 23:
			return make_value< UTCTIME_type >(end_of_len, len_of_msg, input_type);
			break;

		case 24:
			return make_value< GENERALIZEDTIME_type >(end_of_len, len_of_msg, input_type);
			break;

		case 25:
			return make_value< GRAPHICSTRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 26:
			return make_value< VISIBLESTRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 27:
			return make_value< GENERALSTRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 28:
			return make_value< UNIVERSALSTRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 29:
			return make_value< CHARSTRING_type >(end_of_len, len_of_msg, input_type);
			break;

		case 30:
			return make_value< BMPSTRING_type >(end_of_len, len_of_msg, input_type);
			break;
		}
		break;
		case 2: // APPLICATION
		case 3: // CONTEXT-SPECIFIC
		case 4: // PRIVATE
			if (input_type ->id ->get_format()) // if constructed
				return make_value< constr_type >(end_of_len, len_of_msg, input_type);
			else
				return make_value< prim_type >(end_of_len, len_of_msg, input_type);
			break;


	}
	
}

uint8_t* constr_type::decode(uint8_t* input_data, size_t* len_of_msg) {

	if (!(*len_of_msg))
		return input_data;

	uint8_t* input = input_data;

	while (length->get_inform_length() - length_completed()) {
		if (!(*len_of_msg))
			return input_data;

		auto input_type = std::make_shared<type>();

		input = make_type(input, len_of_msg, input_type);
		value.push_back(input_type);

		block_length += input_type->get_length();
	}

	complete = true;

	return input; 
}

std::string constr_type::block_name() {
	std::string str_tag_class;
	switch (id->get_tag_class()) {
	case 1:
		str_tag_class = "UNIVERSAL";
		break;
	case 2:
		str_tag_class = "APPLICATION";
		break;
	case 3:
		str_tag_class = "CONTEXT-SPECIFIC";
		break;
	case 4:
		str_tag_class = "PRIVATE";
		break;
	}
	
	return "[ " + str_tag_class + " " + std::to_string(id->get_tag_number()) + " ] : ";
}

std::vector<std::shared_ptr<type>> constr_type::get_items() {
	return value;
}