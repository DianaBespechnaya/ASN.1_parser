#pragma once

#ifndef SEQUENCE_TYPE_H
#define SEQUENCE_TYPE_H

class SEQUENCE_type : public constr_type {
public:
	SEQUENCE_type();
	std::string block_name();
};

#endif