#include <string>
#include "asn1_types.h"
#include "asn1_exception.h"
#include "CHARSTRING_type.h"

CHARSTRING_type::CHARSTRING_type() {

}

std::string CHARSTRING_type::block_name() {
	return "CHARACTER STRING = " + value;
}

uint8_t* CHARSTRING_type::decode(uint8_t* input_data, size_t* len_of_msg) {
	if (!(*len_of_msg))
		return input_data;

	for (uint8_t octet = 0; octet < length->get_inform_length(); octet++) {
		if (!(*len_of_msg))
			return input_data;

		value += *input_data;

		input_data++;
		block_length++;
		(*len_of_msg)--;
	}

	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}