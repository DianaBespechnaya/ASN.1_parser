#include <cmath>
#include <memory>
#include "transform_functions.h"
#include "asn1_types.h"
#include "asn1_exception.h"
#include "OBJECT_IDENTIFIER_type.h"


OBJECT_IDENTIFIER_type::OBJECT_IDENTIFIER_type() {

}

uint8_t* OBJECT_IDENTIFIER_type::decode(uint8_t* input_data, size_t* len_of_msg) {
	if (!(*len_of_msg))
		return input_data;

	uint64_t first = (*input_data) / 40;
	uint64_t second = (*input_data) % 40;

	value.push_back(first);
	value.push_back(second);

	input_data++;
	block_length++;
	(*len_of_msg)--;

	while (length->get_inform_length() - length_completed()) {
		if (!(*len_of_msg))
			return input_data;

		uint8_t* begin = input_data;
		size_t SID_length = 0; // subidentifier

		while ((*input_data) & 0x80) {
			if (!(*len_of_msg))
				return input_data;

			SID_length++;

			input_data++;
			block_length++;
			(*len_of_msg)--;
		}

		if (!(*len_of_msg))
			return input_data;

		SID_length++;

		input_data++;
		block_length++;
		(*len_of_msg)--;

		uint64_t SID = convert_long_hex(begin, SID_length, 7); // 7 - shift, significant bits

		value.push_back(SID);
	}

	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}

std::string OBJECT_IDENTIFIER_type::block_name() {
	std::string result;

	for (auto bit : value)
		result += std::to_string(bit) + ".";

	if(result.size())
		result.pop_back();

	return "OBJECT_IDENTIFIER = " + result;
}