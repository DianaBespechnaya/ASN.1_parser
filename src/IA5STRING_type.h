#pragma once

#ifndef IA5STRING_TYPE_H
#define IA5STRING_TYPE_H

class IA5STRING_type : public prim_type {
public:
	IA5STRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif