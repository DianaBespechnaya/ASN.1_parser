#include <string>
#include "asn1_types.h"
#include "asn1_exception.h"
#include "GENERALIZEDTIME_type.h"

GENERALIZEDTIME_type::GENERALIZEDTIME_type() {

}

std::string GENERALIZEDTIME_type::block_name() {
	if (value.length() < 15 || value[value.length() - 1] != 'Z')
		throw asn1_exception("DER encoding of GENERALIZEDTIME should be YYYYMMDDHHMMSSZ");
	
	std::string year = value.substr(0, 4);
	std::string month = value.substr(4, 2);
	std::string day = value.substr(6, 2);
	std::string hour = value.substr(8, 2);
	std::string minute = value.substr(10, 2);
	std::string seconds = value.substr(12, 2);

	if (value.length() > 15) { // if fractional seconds
		std::string fractional = value.substr(15, value.length() - 2);
		return "GENERALIZEDTIME = " + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + seconds + "." + fractional;
	}

	return "GENERALIZEDTIME = " + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + seconds;
}

uint8_t* GENERALIZEDTIME_type::decode(uint8_t* input_data, size_t* len_of_msg) {
	if (!(*len_of_msg))
		return input_data;

	for (uint8_t octet = 0; octet < length->get_inform_length(); octet++) {
		if (!(*len_of_msg))
			return input_data;

		value += *input_data;

		input_data++;
		block_length++;
		(*len_of_msg)--;
	}

	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}