﻿#include <cmath>

#include "asn1_types.h"
#include "asn1_exception.h"
#include "REAL_type.h"
#include "transform_functions.h"

REAL_type::REAL_type() : get_special(false), NR_form(0), mantissa(0), exponent(0), base(0){

}

uint8_t* REAL_type::decode(uint8_t* input_data, size_t* len_of_msg) {
	if (!(*len_of_msg))
		return input_data;

	uint8_t bits_8_7 = *input_data & 0xC0;

	switch (bits_8_7) {
	case 0x00: {
		NR_form = *input_data & 0x03;

		if (NR_form != 3)
			throw asn1_exception("Decimal encoding should have NR3 form in DER.");

		std::string dec_mantissa;
		std::string dec_exp;

		bool mantissa_read = true; // reading mantissa, not exp

		input_data++;
		block_length++;
		(*len_of_msg)--;

		for (uint8_t octet = 0; octet < length->get_inform_length() - 1; octet++) { // 1 octet for NR form
			if (!(*len_of_msg))
				return input_data;

			if (*input_data != 'E')
				if (mantissa_read)
					dec_mantissa += *input_data;
				else
					dec_exp += *input_data;
			else
				mantissa_read = false;

			input_data++;
			block_length++;
			(*len_of_msg)--;
		}

		base = 10;
		mantissa = std::stod(dec_mantissa);
		exponent = std::stoi(dec_exp);

		input_data++;
		block_length++;
		(*len_of_msg)--;

		break;
	}

	case 0x40:

		if (*len_of_msg - 2 < 0)
			return input_data;

		if (*input_data & 0x01)
			special_char.push_back('-');
		else
			special_char.push_back('+');

		special_char += "infinity";

		input_data++;
		block_length++;
		(*len_of_msg)--;

		get_special = true;
		break;

	case 0x80:
	case 0xC0: {

		bool sign = *input_data & 0x40; // 0 - pos, 1 - neg

		switch (*input_data & 0x30) {
		case 0x00:
			base = 2;
			break;
		case 0x10:
			base = 8;
			break;
		case 0x20:
			base = 16;
			break;
		default:
			throw asn1_exception("Base encoded as 11 is reserved.");
		}

		switch (*input_data & 0x0C) {
		case 0x00:
			break;
		default:
			throw asn1_exception("REAL encoding should have scaling factor = 0 in DER.");
		}

		//decodind exponenta

		uint8_t exp_length;

		switch (*input_data & 0x03) {
		case 0:
			exp_length = 1;
			break;
		case 1:
			exp_length = 2;
			break;
		case 2:
			exp_length = 3;
			break;
		case 3:
			input_data++;
			block_length++;
			(*len_of_msg)--;

			if (!(*len_of_msg))
				return input_data;

			exp_length = *input_data;
			break;
		}

		uint8_t* begin = ++input_data;
		block_length++;
		(*len_of_msg)--;

		(*len_of_msg) -= exp_length;
		block_length += exp_length;
		input_data += exp_length;

		if (len_of_msg < 0)
			return input_data;

		if (*begin & 0x80) // sign of exp
			exponent = convert_long_neg_hex(begin, exp_length); 
		else
			exponent = convert_long_hex(begin, exp_length, 8); // 8 - significant bits

		//decoding mantissa

		if (*len_of_msg < length->get_inform_length() - length_completed()) // check if the mantissa is not full
			return input_data;

		uint8_t rest_length = length->get_inform_length() - length_completed();

		begin = input_data;
		(*len_of_msg) -= rest_length;
		block_length += rest_length;
		input_data += rest_length;

		if (sign)
			mantissa = convert_long_neg_hex(begin, rest_length);
		else
			mantissa = convert_long_hex(begin, rest_length, 8); // 8 - significant bits 
		break;
	}
	}

	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}

std::string REAL_type::block_name() {
	if (get_special)
		return "REAL = " + special_char;
	else
		return "REAL = " + std::to_string(mantissa) + " * " + std::to_string(base) + " ^ " + std::to_string(exponent);
}
