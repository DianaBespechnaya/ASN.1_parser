#pragma once

#ifndef VISIBLESTRING_TYPE_H
#define VISIBLESTRING_TYPE_H

class VISIBLESTRING_type : public prim_type {
public:
	VISIBLESTRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::string value;
};

#endif