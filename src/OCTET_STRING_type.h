#pragma once

#ifndef OCTET_STRING_TYPE_H
#define OCTET_STRING_TYPE_H

class OCTET_STRING_type : public prim_type {
public:
	OCTET_STRING_type();
	std::string block_name();
	uint8_t* decode(uint8_t*, size_t*);
private:
	std::vector<bool> value;
};

#endif