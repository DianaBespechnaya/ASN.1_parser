#include <cmath>

#include "asn1_types.h"
#include "asn1_exception.h"
#include "BIT_STRING_type.h"

BIT_STRING_type::BIT_STRING_type() {

}

std::string BIT_STRING_type::block_name() {
	std::string result;

	for (auto bit : value)
		result += std::to_string(bit);
	return "BIT_STRING = " + result;
}

uint8_t* BIT_STRING_type::decode(uint8_t* input_data, size_t* len_of_msg) {
	if (!(*len_of_msg))
		return input_data;

	uint8_t insignificant_bits = *input_data;

	input_data++;
	block_length++;
	(*len_of_msg)--;

	for (uint8_t octet = 1; octet < length->get_inform_length(); octet++) {
		if (!(*len_of_msg))
			return input_data;

		uint8_t bits_to_read = 8;

		if (octet == length->get_inform_length() - 1)
			bits_to_read = 8 - insignificant_bits;

		for (uint8_t bit = 0; bit < bits_to_read; bit++) {
			uint8_t position = pow(2, 7 - bit);
			bool current_bit = (*input_data) & position;
			value.push_back(current_bit);
		}

		input_data++;
		block_length++;
		(*len_of_msg)--;
	}

	complete = true;

	uint8_t* end_of_data = input_data;

	return end_of_data;
}